﻿using Desktop_Exercise_5.Interfaces;
using Desktop_Exercise_5.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop_Exercise_5.Models
{
  public class Sport : ISport
  {
    public Sport(int numberOfPlayersIn, string sportNameIn, Lookups.SportCategory sportCategoryIn)
    {
      NumberOfPlayers = numberOfPlayersIn;
      _sportName = sportNameIn;
      _sportCategory = sportCategoryIn;

    }
    /// <summary>
    /// Private Variables
    /// </summary>
    /// 
    private string _sportName;
    private Lookups.SportCategory _sportCategory;
    /// <summary>
    /// Public Property
    /// </summary>
    /// 
    public int NumberOfPlayers { get; set; }

    public Lookups.SportCategory SportCategory
    {
      get { return _sportCategory; }
    }

    public string SportName
    {
      get { return _sportName; }
    }
    /// <summary>
    /// Public Method
    /// </summary>
    /// 

    public decimal Length { get; set; }

    public decimal Width { get; set; }

    public void WriteSportProperties()
    {
      Console.WriteLine("Sport Properties");
      Console.WriteLine("Name: {0}", _sportName);
      Console.WriteLine("Category: {0}", _sportCategory);
      Console.WriteLine("# of Players: {0} ", NumberOfPlayers);
      Console.WriteLine("Court Dimensions: {0} x {1}", Length, Width);
      Console.WriteLine();
    }
  }
}
