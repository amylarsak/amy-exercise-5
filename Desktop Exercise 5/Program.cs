﻿using System;
using System.Collections.Generic;
using Desktop_Exercise_5.Utility;
using Desktop_Exercise_5.Models;
using Desktop_Exercise_5.Interfaces;


namespace Desktop_Exercise_5
{
  class Program
  {
    static void Main(string[] args)
    {
      Sport volleyBall = new Sport(6, "Volleyball", Lookups.SportCategory.Team);
      volleyBall.Length = 60;
      volleyBall.Width = 30;
      volleyBall.WriteSportProperties();

      Sport baseBall = new Sport(10, "Baseball", Lookups.SportCategory.Team);
      baseBall.Length = 250;
      baseBall.Width = 400;
      baseBall.WriteSportProperties();

      Console.ReadKey();
    }
  }
}
